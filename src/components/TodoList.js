import React, { useState, useRef } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { v4 as uuid4 } from 'uuid';


const TodoList = () => {

    const [tasks, setTasks] = useState([])
    const [newTask, setNewTask] = useState("")
    const [listShow, setListShow] = useState("all")
    const [selectedDate, setSelectedDate] = useState(null);
    const [modalText, setModalText] = useState(null)
    const modalRef = useRef(null)
    const modalRefC = useRef(null)

    const addTask = () => {

        if(newTask.length <10){
            alert("Task Should be Min of 10 characters")
            return
        }

        if(selectedDate == null){
            alert("Select the Date as well")
            return
        }

        const formattedDate = selectedDate.toLocaleDateString('en-GB', {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
        });

        const Task = {
            id: uuid4(),
            text: newTask,
            status: 'pending',
            date: formattedDate,
        };

        setTasks([...tasks, Task].sort((a, b) => {
            const dateA = a.date.split('/').reverse().join('/');
            const dateB = b.date.split('/').reverse().join('/');
            return new Date(dateA) - new Date(dateB);
          }));

        setNewTask("");
        setSelectedDate(null);
    }

    const toggleClick = (e) => {
        setListShow(e.target.id);
    }


    const toggleTask = (taskId) =>{
        setTasks(tasks.map(task =>
            task.id === taskId ? { ...task, status: (task.status === 'pending')?'completed':'pending'} : task
        ));
    } 
    
    const deleteTask = (taskId) => {
        setTasks(tasks.filter(task => task.id !== taskId));
    };

    const openModal = (taskId, taskText) =>{
        setModalText({
            text: taskText,
            id: taskId
        });
        modalRef.current.click();
    }

    const handleDateChange = (date) =>{
        if(date){
            setSelectedDate(date);
        }
    }

    

  return (
<>
        

        <button type="button" ref={modalRef} class="btn btn-primary d-none" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
        Launch static backdrop modal
        </button>


        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="staticBackdropLabel">Delete Task ?</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-left">
                        <b>Are your sure you want to delete Task ?</b>
                        <p className='mt-2 mb-0'> <b>ID</b> : {modalText.id}</p>
                        <p><b>Text</b> : {modalText.text}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" ref={modalRefC} class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" onClick={()=>{deleteTask(modalText); modalRefC.current.click()}}>Delete</button>
                </div>
                </div>
            </div>
        </div>
        
        
         <div className="p-4 bg-dark" style={{minHeight: "100vh"}}>

         
          
          <div className="card mt-5 m-auto"style={{width: "70%"}}>
            <div className="card-header row justify-content-between align-items-center">
              <div className="col-12 mb-2 fs-3" style={{color:"#212529"}}><h2>Todo List</h2></div>
              <div className="col-2"><p className="fs-5">{tasks.length} Tasks</p></div>
              <div className="col-5 task-filters">
                <button className={`btn toggle-buttons ${listShow === "all" ? 'bg-dark text-light': ''} `} id="all" onClick={toggleClick}>All</button>
                <button className={`btn toggle-buttons ${listShow === "pending" ? 'bg-dark text-light': ''} `} id="pending" onClick={toggleClick}>Active</button>
                <button className={`btn toggle-buttons ${listShow === "completed" ? 'bg-dark text-light': ''} `} id="completed" onClick={toggleClick}>Completed</button>
              </div>
            </div>
            <div className="card-body">
                <div className="row align-items-end mb-4">
                    <div className="col-8">
                        <input type="text" className='form-control' value={newTask} onChange={(e) => setNewTask(e.target.value)} placeholder="Add a new task" />
                    </div>
                    <div className='col-2'>
                        <DatePicker
                            className='form-control'
                            selected={selectedDate}
                            onChange={handleDateChange}
                            dateFormat="dd/MM/yyyy"
                            isClearable
                            placeholderText="Select a date"
                        />
                    </div>
                    <div className="col-2">
                    <button className="btn btn-primary mt-2" style={{backgroundColor:"#212529"}}onClick={addTask}> <i class="fa-solid fa-plus"></i> </button>
                    </div>
                </div>
              <div className="list-group list-group-flush list-group-hoverable">
                {tasks.filter((item) => (listShow==='all' || item.status === listShow)).map((item) => {
                  return(
                    <div className="list-group-item px-0" key={item._id} style={{backgroundColor:(item.status === 'completed' ? '#27b62747' : '#9a17502e')}}>
                      <div className="row align-items-center">
                        <div className="col-1">
                            <input type="checkbox" className='form-check-input' checked={item.status !== 'pending'} onChange={() => toggleTask(item.id)} />
                        </div>
                        <div className="col-7">
                          <div className={`d-block text-muted ${item.status === 'completed' ? 'text-decoration-line-through' : ''}`}>
                          <p className="mb-0 fs-6 gap-2 d-flex">
                            {item.text}
                          </p>
                          </div>
                        </div>
                        <div className="col-2">
                          <div className={`d-block text-muted ${item.status === 'completed' ? 'text-decoration-line-through' : ''}`}>
                          <p className="mb-0 fs-6 gap-2 d-flex">
                            {item.date}
                          </p>
                          </div>
                        </div>
                        <div className="col-2" >
                          <button className="btn text-light fs-6" onClick={()=>{openModal(item.id, item.text)}}><i style={{color: '#5a1734'}} class="fa-solid fs-5 fa-trash"></i></button>
                        </div>
                      </div>
                    </div>);
                })}
              </div>
              
            </div>
          </div>
          
        </div>
        </>
    );
};

export default TodoList;